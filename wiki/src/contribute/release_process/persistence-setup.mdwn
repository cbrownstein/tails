[[!meta title="Testing persistence-setup"]]

[[!toc levels=1]]

Everything on this page happens in the
`config/chroot_local-includes/usr/src/persistence-setup` directory.

Pre-requisites
=============

* a Debian Stretch (or newer) system
* [[Install perl5lib's dependencies|perl5lib#build-deps]]
* Environment:
  - Export location of a checkout of the `stable` branch of the main
    Tails Git repository:

        export TAILS_GIT_CHECKOUT="/path/to/your/tails/git/repo"

Install test dependencies
=========================

    cat $(git rev-parse --show-toplevel)/config/chroot_local-packageslists/tails-persistence-setup.list \
       | grep -E -v '^#' \
       | xargs sudo apt --yes install && \
    sudo apt install \
       libdist-zilla-plugin-test-notabs-perl \
       libdist-zilla-plugin-test-perl-critic-perl \
       libdist-zilla-app-command-authordebs-perl && \
    dzil authordebs --install

Run the test suite
==================

    PERL5LIB="${TAILS_GIT_CHECKOUT}/config/chroot_local-includes/usr/src/perl5lib/lib" \
       RELEASE_TESTING=1 \
       LC_ALL=C \
       fakeroot dzil test

It has happened in the past that this code base required
adjustments to work with newer versions of the libraries it depends
on, so if tests fail on testing/sid, retry on Debian stable before
going amok.
