Hi!

We've set up an automated process to ask our fellow contributors to update some tickets of theirs, in order to:

* better reflect your plans;

* bring down your amount of work-in-progress to a sustainable level;

* encourage team work and increase the chances that someone finishes the work;

* avoid a human doing ticket triaging and asking you the same questions on each such ticket.

In particular, this process identifies:

* Stalled work-in-progress

* Reviews waiting for a long time

However, in the current state of things, this process is not able to notice those tickets when their Target version has been repeatedly postponed by our Release Managers. Therefore, the ticket triaging team decided on #16545 to remove the Target version whenever in such cases, when it does not feel realistic. This is what I'm doing on this ticket.

You now have a few options, such as:

* Deassign yourself. That's fine. If it really matters, someone else, possibly you, may pick it up later. Then, if this ticket is relevant for a Tails team, bring it to their attention; else, forget it and take care of yourself :)

* If you think you can realistically come back to it and finish the work in the next 6 months, say so on this ticket, for example by setting a suitable "Target version". This will communicate your plans to the rest of the project and ensure the task pops up on your radar at a suitable time. Of course, you can still realize later that it is not going to work as planned, and revisit today's choice.

Cheers!
