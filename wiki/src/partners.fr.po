# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2018-05-30 13:55+0200\n"
"PO-Revision-Date: 2018-03-22 02:01+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"

#. type: Content of: outside any tag (error?)
msgid "[[!meta title=\"Partners\"]]"
msgstr "[[!meta title=\"Partenaires\"]]"

#. type: Content of: <p>
msgid ""
"These organizations, companies and individuals provide financial support to "
"Tails through grants or donations. Thanks to their substantial support we "
"are able to maintain and improve Tails. Thank you!"
msgstr ""
"Ces associations, entreprises et individus apportent un soutien financier à "
"Tails au travers de subventions ou de dons. Grâce à leur important soutien "
"nous pouvons maintenir et améliorer Tails. Merci !"

#. type: Content of: <h1>
msgid "Becoming a partner"
msgstr "Devenir partenaire"

#. type: Content of: <p>
msgid ""
"Are you a company or organization and want to help Tails? [[Become a "
"partner!|partners/become/]]"
msgstr ""
"Vous être une entreprise ou une association et vous voulez aider Tails ? "
"[[Devenez partenaire !|partners/become/]]"

#. type: Content of: <p>
msgid ""
"Are you an individual and want to help Tails? [[We warmly welcome your "
"donations!|donate]]"
msgstr ""
"Vous êtes un individu et vous voulez aider Tails ? [[Nous accueillons "
"chaleureusement vos dons !|donate]]"

#. type: Content of: <h1>
msgid "Current partners"
msgstr "Partenaires actuels"

#. type: Content of: <p>
#, fuzzy
#| msgid "&gt $100.000"
msgid "&gt $100&#8239;000"
msgstr "&gt 100 000$"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Open Technology Fund - $132.390"
msgid "Open Technology Fund - $132 390"
msgstr "Open Technology Fund - 132 390$"

#. type: Content of: <div><p>
msgid "[[!img lib/partners/otf.png link=\"no\"]]"
msgstr "[[!img lib/partners/otf.png link=\"no\"]]"

#. type: Content of: <p>
#, fuzzy
#| msgid "$10.000 – $50.000"
msgid "$10&#8239;000 – $50&#8239;000"
msgstr "10 000$ – 50 000$"

#. type: Attribute 'title' of: <div><p>
msgid "DuckDuckGo donation challenge - $38 080"
msgstr ""

#. type: Content of: <div><p>
msgid "[[!img lib/partners/ddg.png link=\"no\"]]"
msgstr "[[!img lib/partners/ddg.png link=\"no\"]]"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Lush Digital Fund - 11.000€"
msgid "Lush Digital Fund - 11 000€"
msgstr "Lush Digital Fund - 11 000€"

#. type: Content of: <div><p>
msgid "[[!img lib/partners/lush.png link=\"no\"]]"
msgstr "[[!img lib/partners/lush.png link=\"no\"]]"

#. type: Content of: <p>
#, fuzzy
#| msgid "$1.000 – $9.999"
msgid "$1&#8239;000 – $9&#8239;999"
msgstr "1 000$ – 9 999$"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "An individual - $2.000"
msgid "An individual - $2 000"
msgstr "Un individu - 2 000$"

#. type: Content of: <div><p>
msgid "[[!img lib/partners/anonymous.png link=\"no\"]]"
msgstr "[[!img lib/partners/anonymous.png link=\"no\"]]"

#. type: Content of: <div><p>
msgid ""
"<a href=\"https://geti2p.net/\" title=\"I2P - 0.1BTC\">[[!img lib/partners/"
"i2p.png link=\"no\"]]</a>"
msgstr ""
"<a href=\"https://geti2p.net/fr/\" title=\"I2P - 0,1BTC\">[[!img lib/"
"partners/i2p.png link=\"no\"]]</a>"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "An individual - $2.000"
msgid "An individual - $1 000"
msgstr "Un individu - 2 000$"

#. type: Content of: <p>
msgid "in-kind"
msgstr "en nature"

#. type: Attribute 'title' of: <div><p>
msgid "Tor - Server rent"
msgstr "Tor - Location de serveur"

#. type: Content of: <div><p>
msgid ""
"<a href=\"https://torproject.org\">[[!img lib/partners/tor.png link=\"no"
"\"]]</a>"
msgstr ""
"<a href=\"https://torproject.org\">[[!img lib/partners/tor.png link=\"no"
"\"]]</a>"

#. type: Content of: <h1>
msgid "[[!toggle id=\"previous_partners\" text=\"Previous partners\"]]"
msgstr "[[!toggle id=\"previous_partners\" text=\"Précédents partenaires\"]]"

#. type: Content of: outside any tag (error?)
msgid ""
"[[!toggleable id=\"previous_partners\" text=\"\"\" <span class=\"hide\">[[!"
"toggle id=\"previous_partners\" text=\" \"]]</span> <span class=\"clearfix"
"\"></span>"
msgstr ""
"[[!toggleable id=\"previous_partners\" text=\"\"\" <span class=\"hide\">[[!"
"toggle id=\"previous_partners\" text=\" \"]]</span> <span class=\"clearfix"
"\"></span>"

#. type: Content of: <h2>
msgid "2017"
msgstr "2017"

#. type: Content of: <p>
#, fuzzy
#| msgid "$50.000 – $99.000"
msgid "$50&#8239;000 – $99&#8239;000"
msgstr "50 000$ – 99 000$"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Mozilla Open Source Support - $77.000"
msgid "Mozilla Open Source Support - $77 000"
msgstr "Mozilla Open Source Support - 77 000$"

#. type: Content of: <div><p>
msgid "[[!img lib/partners/mozilla.png link=\"no\"]]"
msgstr "[[!img lib/partners/mozilla.png link=\"no\"]]"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "ExpressVPN - $1.000"
msgid "ExpressVPN - $1 000"
msgstr "ExpressVPN - 1 000$"

#. type: Content of: <div><p>
msgid "[[!img lib/partners/expressvpn.png link=\"no\"]]"
msgstr "[[!img lib/partners/expressvpn.png link=\"no\"]]"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Tor - Travel to Tor dev meeting & server rent"
msgid "Tor - Travel to Tor meeting & server rent"
msgstr "Tor - Transport pour le Tor dev meeting et location de serveur"

#. type: Content of: <div><p>
msgid "[[!img lib/partners/tor.png link=\"no\"]]"
msgstr "[[!img lib/partners/tor.png link=\"no\"]]"

#. type: Content of: outside any tag (error?)
msgid "<span class=\"clearfix\"></span>"
msgstr "<span class=\"clearfix\"></span>"

#. type: Content of: <h2>
msgid "2016"
msgstr "2016"

#. type: Content of: <p>
#, fuzzy
#| msgid "&gt; $100.000"
msgid "&gt; $100&#8239;000"
msgstr "&gt; 100 000$"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Open Technology Fund - $208.800"
msgid "Open Technology Fund - $208 800"
msgstr "Open Technology Fund - 208 800$"

#. type: Content of: <p>
#, fuzzy
#| msgid "$10.000 – $49.999"
msgid "$10&#8239;000 – $49&#8239;999"
msgstr "10 000$ – 49 999$"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Anonymous donation - 11.609€"
msgid "Anonymous donation - 11 609€"
msgstr "Don anonyme - 11 609€"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "An individual - $5.000"
msgid "An individual - $5 000"
msgstr "Un individu - 5 000$"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Anonymous donation - 2.902€"
msgid "Anonymous donation - 2 902€"
msgstr "Don anonyme - 2 902€"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Mediapart - 2.000€"
msgid "Mediapart - 2 000€"
msgstr "Mediapart - 2 000€"

#. type: Content of: <div><p>
msgid "[[!img lib/partners/mediapart.png link=\"no\"]]"
msgstr "[[!img lib/partners/mediapart.png link=\"no\"]]"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "An individual - 2.000€"
msgid "An individual - 2 000€"
msgstr "Un individu - 2 000€"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Anonymous donation - 2.554€"
msgid "Anonymous donation - 2 554€"
msgstr "Don anonyme - 2 554€"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Anonymous donation - 1.857€"
msgid "Anonymous donation - 1 857€"
msgstr "Don anonyme - 1 857€"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Anonymous donation - 1.433€"
msgid "Anonymous donation - 1 433€"
msgstr "Don anonyme - 1 433€"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Anonymous donation - 1.161€"
msgid "Anonymous donation - 1 161€"
msgstr "Don anonyme - 1 161€"

#. type: Attribute 'title' of: <div><p>
msgid "Google - GSoC Tails Server"
msgstr "Google - GSoC Tails Server"

#. type: Content of: <div><p>
msgid "[[!img lib/partners/gsoc.png link=\"no\"]]"
msgstr "[[!img lib/partners/gsoc.png link=\"no\"]]"

#. type: Attribute 'title' of: <div><p>
msgid "Tor - Travel sponsorship & server rent"
msgstr "Tor - Financement de transports et location de serveur"

#. type: Content of: <h2>
msgid "2015"
msgstr "2015"

#. type: Content of: <p>
#, fuzzy
#| msgid "$50.000 – $99.999"
msgid "$50&#8239;000 – $99&#8239;999"
msgstr "50 000$ – 99 999$"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Hivos International - 70.000€"
msgid "Hivos International - 70 000€"
msgstr "Hivos International - 70 000€"

#. type: Content of: <div><p>
msgid "[[!img lib/partners/hivos.png link=\"no\"]]"
msgstr "[[!img lib/partners/hivos.png link=\"no\"]]"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "DuckDuckGo - $25.000"
msgid "DuckDuckGo - $25 000"
msgstr "DuckDuckGo - 25 000$"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Laura Poitras & Edward Snowden, Ridenhour's film award - $10.000"
msgid "Laura Poitras & Edward Snowden, Ridenhour's film award - $10 000"
msgstr ""
"Laura Poitras et Edward Snowden, prix Ridenhour du documentaire - 10 000$"

#. type: Content of: <div><p>
msgid "Laura Poitras &"
msgstr "Laura Poitras et"

#. type: Content of: <div><p>
msgid "Edward Snowden"
msgstr "Edward Snowden"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Freedom of the Press Foundation - $8.859"
msgid "Freedom of the Press Foundation - $8 859"
msgstr "Freedom of the Press Foundation - 8 859$"

#. type: Content of: <div><p>
msgid "[[!img lib/partners/fpf.png link=\"no\"]]"
msgstr "[[!img lib/partners/fpf.png link=\"no\"]]"

#. type: Attribute 'title' of: <div><p>
msgid "Localization Lab - Translation to Farsi"
msgstr "Localization Lab - Traduction en persan"

#. type: Content of: <div><p>
msgid "[[!img lib/partners/localizationlab.png link=\"no\"]]"
msgstr "[[!img lib/partners/localizationlab.png link=\"no\"]]"

#. type: Content of: <h2>
msgid "2014"
msgstr "2014"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Access Now - 50.000€"
msgid "Access Now - 50 000€"
msgstr "Access Now - 50 000€"

#. type: Content of: <div><p>
msgid "[[!img lib/partners/accessnow.png link=\"no\"]]"
msgstr "[[!img lib/partners/accessnow.png link=\"no\"]]"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Freedom of the Press Foundation - $33.377"
msgid "Freedom of the Press Foundation - $33 377"
msgstr "Freedom of the Press Foundation - 33 377$"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Open Internet Tools - $25.800"
msgid "Open Internet Tools - $25 800"
msgstr "Open Internet Tools - 25 800$"

#. type: Content of: <div><p>
msgid "[[!img lib/partners/openitp.png link=\"no\"]]"
msgstr "[[!img lib/partners/openitp.png link=\"no\"]]"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Förderung Freier Information und Software - 5.000€"
msgid "Förderung Freier Information und Software - 5 000€"
msgstr "Förderung Freier Information und Software - 5 000€"

#. type: Content of: <div><p>
msgid "[[!img lib/partners/ffis.png link=\"no\"]]"
msgstr "[[!img lib/partners/ffis.png link=\"no\"]]"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Debian - $5.000"
msgid "Debian - $5 000"
msgstr "Debian - 5 000$"

#. type: Content of: <div><p>
msgid "[[!img lib/partners/debian.png link=\"no\"]]"
msgstr "[[!img lib/partners/debian.png link=\"no\"]]"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Tor - $5.000"
msgid "Tor - $5 000"
msgstr "Tor - 5 000$"

#. type: Attribute 'title' of: <div><p>
msgid "Mozilla - Travel to Tails hackfest"
msgstr "Mozilla - Transport pour le Tails hackfest"

#. type: Content of: <h2>
msgid "2013"
msgstr "2013"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "National Democratic Institute - $21.000"
msgid "National Democratic Institute - $21 000"
msgstr "National Democratic Institute - 21 000$"

#. type: Content of: <div><p>
msgid "[[!img lib/partners/ndi.png link=\"no\"]]"
msgstr "[[!img lib/partners/ndi.png link=\"no\"]]"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Tor - $20.000"
msgid "Tor - $20 000"
msgstr "Tor - 20 000$"

#. type: Content of: <h2>
msgid "2012"
msgstr "2012"

#. type: Content of: <h2>
msgid "2011"
msgstr "2011"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Tor - $10.950"
msgid "Tor - $10 950"
msgstr "Tor - 10 950$"

#. type: Content of: <h2>
msgid "2010"
msgstr "2010"

#. type: Attribute 'title' of: <div><p>
#, fuzzy
#| msgid "Tor - 8.500€"
msgid "Tor - 8 500€"
msgstr "Tor - 8 500€"

#. type: Content of: outside any tag (error?)
msgid "\"\"\"]]"
msgstr "\"\"\"]]"

#~ msgid ""
#~ "<a href=\"https://www.opentech.fund/\" title=\"Open Technology Fund - "
#~ "$132.390\">[[!img lib/partners/otf.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.opentech.fund/\" title=\"Open Technology Fund - 132 "
#~ "390$\">[[!img lib/partners/otf.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.lush.com/\" title=\"Lush Digital Fund - 11.000€"
#~ "\">[[!img lib/partners/lush.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.lush.com/\" title=\"Lush Digital Fund - 11 000€"
#~ "\">[[!img lib/partners/lush.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.mozilla.org/moss/\" title=\"Mozilla Open Source "
#~ "Support - $77.000\">[[!img lib/partners/mozilla.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.mozilla.org/moss/\" title=\"Mozilla Open Source "
#~ "Support - 77 000$\">[[!img lib/partners/mozilla.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.expressvpn.com/\" title=\"ExpressVPN - $1.000\">[[!"
#~ "img lib/partners/expressvpn.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.expressvpn.com/fr\" title=\"ExpressVPN - 1 000$"
#~ "\">[[!img lib/partners/expressvpn.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.torproject.org/\" title=\"Tor - Travel to Tor dev "
#~ "meeting & server rent\">[[!img lib/partners/tor.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.torproject.org/\" title=\"Tor - Transport pour le "
#~ "Tor dev meeting et location de serveur\">[[!img lib/partners/tor.png link="
#~ "\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.opentech.fund/\" title=\"Open Technology Fund - "
#~ "$208.800\">[[!img lib/partners/otf.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.opentech.fund/\" title=\"Open Technology Fund - 208 "
#~ "800$\">[[!img lib/partners/otf.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.mediapart.fr\" title=\"Mediapart - 2.000€\">[[!img "
#~ "lib/partners/mediapart.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.mediapart.fr\" title=\"Mediapart - 2 000€\">[[!img "
#~ "lib/partners/mediapart.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://summerofcode.withgoogle.com/\" title=\"Google - GSoC "
#~ "Tails Server\">[[!img lib/partners/gsoc.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://summerofcode.withgoogle.com/\" title=\"Google - GSoC "
#~ "Tails Server\">[[!img lib/partners/gsoc.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.torproject.org/\" title=\"Tor - Travel sponsorship "
#~ "& server rent\">[[!img lib/partners/tor.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.torproject.org/\" title=\"Tor - Financement de "
#~ "transports et location de serveur\">[[!img lib/partners/tor.png link=\"no"
#~ "\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.hivos.org/\" title=\"Hivos International - 70.000€"
#~ "\">[[!img lib/partners/hivos.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.hivos.org/\" title=\"Hivos International - 70 000€"
#~ "\">[[!img lib/partners/hivos.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://duckduckgo.com/\" title=\"DuckDuckGo - $25.000\">[[!img "
#~ "lib/partners/ddg.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://duckduckgo.com/\" title=\"DuckDuckGo - 25 000$\">[[!img "
#~ "lib/partners/ddg.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.freedom.press/\" title=\"Freedom of the Press "
#~ "Foundation - $8.859\">[[!img lib/partners/fpf.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.freedom.press/\" title=\"Freedom of the Press "
#~ "Foundation - 8 859$\">[[!img lib/partners/fpf.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"http://www.localizationlab.org/\" title=\"Localization Lab - "
#~ "Translation to Farsi\">[[!img lib/partners/localizationlab.png link=\"no"
#~ "\"]]</a>"
#~ msgstr ""
#~ "<a href=\"http://www.localizationlab.org/\" title=\"Localization Lab - "
#~ "Traduction en persan\">[[!img lib/partners/localizationlab.png link=\"no"
#~ "\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.torproject.org/\" title=\"Tor - Server rent\">[[!"
#~ "img lib/partners/tor.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.torproject.org/\" title=\"Tor - Location de serveur"
#~ "\">[[!img lib/partners/tor.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.accessnow.org/\" title=\"Access Now - 50.000€\">[[!"
#~ "img lib/partners/accessnow.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.accessnow.org/\" title=\"Access Now - 50 000€\">[[!"
#~ "img lib/partners/accessnow.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.freedom.press/\" title=\"Freedom of the Press "
#~ "Foundation - $33.377\">[[!img lib/partners/fpf.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.freedom.press/\" title=\"Freedom of the Press "
#~ "Foundation - 33 377$\">[[!img lib/partners/fpf.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.opentech.fund/project/open-internet-tools-project\" "
#~ "title=\"Open Internet Tools - $25.800\">[[!img lib/partners/openitp.png "
#~ "link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.opentech.fund/project/open-internet-tools-project\" "
#~ "title=\"Open Internet Tools - 25 800$\">[[!img lib/partners/openitp.png "
#~ "link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.ffis.de/\" title=\"Förderung Freier Information und "
#~ "Software - 5.000€\">[[!img lib/partners/ffis.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.ffis.de/\" title=\"Förderung Freier Information und "
#~ "Software - 5 000€\">[[!img lib/partners/ffis.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.debian.org/\" title=\"Debian - $5.000\">[[!img lib/"
#~ "partners/debian.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.debian.org/index.fr.html\" title=\"Debian - 5 000$"
#~ "\">[[!img lib/partners/debian.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.mozilla.org/\" title=\"Mozilla - Travel to Tails "
#~ "hackfest\">[[!img lib/partners/mozilla.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.mozilla.org/fr/\" title=\"Mozilla - Transport pour "
#~ "le Tails hackfest\">[[!img lib/partners/mozilla.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"http://www.ndi.org/\" title=\"National Democratic Institute - "
#~ "$21.000\">[[!img lib/partners/ndi.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"http://www.ndi.org/\" title=\"National Democratic Institute - "
#~ "21 000$\">[[!img lib/partners/ndi.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.torproject.org/\" title=\"Tor - $20.000\">[[!img "
#~ "lib/partners/tor.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.torproject.org/\" title=\"Tor - 20 000$\">[[!img "
#~ "lib/partners/tor.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.torproject.org/\" title=\"Tor - $10.950\">[[!img "
#~ "lib/partners/tor.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.torproject.org/\" title=\"Tor - 10 950$\">[[!img "
#~ "lib/partners/tor.png link=\"no\"]]</a>"

#~ msgid ""
#~ "<a href=\"https://www.torproject.org/\" title=\"Tor - 8.500€\">[[!img lib/"
#~ "partners/tor.png link=\"no\"]]</a>"
#~ msgstr ""
#~ "<a href=\"https://www.torproject.org/\" title=\"Tor - 8 500€\">[[!img lib/"
#~ "partners/tor.png link=\"no\"]]</a>"
