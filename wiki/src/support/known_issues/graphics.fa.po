# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-09-04 10:11+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Known issues with graphics cards\"]]\n"
msgstr ""

#. type: Plain text
msgid ""
"- For other hardware compatibility issues, refer to our [[known issues|"
"support/known_issues]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr "[[!toc levels=2]]\n"

#. type: Title =
#, no-wrap
msgid "Error starting GDM\n"
msgstr ""

#. type: Plain text
msgid ""
"This section applies if you see the following error message when starting "
"Tails:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p class=\"pre\">\n"
"Error starting GDM with your graphics card: <i>name of your graphics\n"
"card [id] (rev number)</i>. Please take note of this error and visit\n"
"https://tails.boum.org/gdm for troubleshooting.\n"
"</p>\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Identify the name, ID, and revision number (if any) of your graphics card."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   For example, if your error message starts with:\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <p class=\"pre\">Error starting GDM with your graphics card:\n"
"   NVIDIA Corporation GT218M [NVS 3100M] [10de:0a6c] (rev a2)</p>\n"
msgstr ""
"   <p class=\"pre\">Error starting GDM with your graphics card:\n"
"   NVIDIA Corporation GT218M [NVS 3100M] [10de:0a6c] (rev a2)</p>\n"

#. type: Bullet: '   - '
msgid "The name is *NVIDIA Corporation GT218M [NVS 3100M]*."
msgstr ""

#. type: Bullet: '   - '
msgid ""
"The ID is *[10de:0a6c]*. The ID is unique to the model of your graphics "
"card, it is not unique to your computer."
msgstr ""

#. type: Bullet: '   - '
msgid ""
"The revision number is *a2*. Your graphics card might have no revision "
"number."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Check if your graphics card is listed below. For example, you can search for "
"its name or ID on this page."
msgstr ""

#. type: Bullet: '   - '
msgid ""
"If your graphics card is listed, check if a workaround is documented to make "
"it work on Tails."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"     If the workaround doesn't work, please [[report the problem to our\n"
"     help desk|support/talk]].\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "     Mention in your email:\n"
msgstr ""

#. type: Bullet: '     - '
msgid "The version of Tails that you are trying to start."
msgstr ""

#. type: Bullet: '     - '
msgid "The name, ID, and revision number (if any) of your graphics card."
msgstr ""

#. type: Bullet: '     - '
msgid "The workaround that you tried and that failed."
msgstr ""

#. type: Bullet: '   - '
msgid ""
"If your graphics card is not listed, please [[contact our support team by "
"email|support/talk]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "     <div class=\"tip\">\n"
msgstr "     <div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"     <p>You can send us a photo of the error message as it appears on\n"
"     your screen.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "     </div>\n"
msgstr "     </div>\n"

#. type: Bullet: '1. '
msgid ""
"If your problem get fixed in a future version of Tails, please let us know "
"so we can update this page."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<!--\n"
msgstr "<!--\n"

#. type: Title =
#, no-wrap
msgid "Name and ID in /usr/share/misc/pci.ids\n"
msgstr ""

#. type: Plain text
msgid ""
"The correspondence between the name and ID is established in /usr/share/misc/"
"pci.ids."
msgstr ""

#. type: Plain text
msgid "For example:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"\t8086  Intel Corporation\n"
"\t        0007  82379AB\n"
"\t        [...]\n"
"\t        0046  Core Processor Integrated Graphics Controller\n"
msgstr ""

#. type: Plain text
msgid "Corresponds to:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "\tIntel Corporation Core Processor Integrated Graphics Controller [8086:0046]\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Template for new section\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"$ANCHOR\"></a>\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "$FAMILY_NAME\n"
msgstr ""

#. type: Plain text
msgid "$LT!-- Tickets: #XXXXX #XXXXX --$GT"
msgstr ""

#. type: Title ###
#, no-wrap
msgid "Affected graphics cards"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>\n"
"<tr><td>$VENDOR $DEVICE</td><td>[$VENDOR_ID:$PRODUCT_ID]</td><td>(rev $REVISION_NUMBER)</td></tr>\n"
"</table>\n"
msgstr ""

#. type: Title ###
#, no-wrap
msgid "Workaround"
msgstr ""

#. type: Plain text
msgid "$WORKAROUND_IF_ANY"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "-->\n"
msgstr "-->\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"radeon-hd\"></a>\n"
msgstr "<a id=\"radeon-hd\"></a>\n"

#. type: Title -
#, no-wrap
msgid "AMD Radeon HD\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<!--\n"
"Tickets: #11095 #12482\n"
"-->\n"
msgstr ""
"<!--\n"
"Tickets: #11095 #12482\n"
"-->\n"

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>\n"
"<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] Mars XTX [Radeon HD 8790M]</td><td>[1002:6606]</td><td></td></tr>\n"
"<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] Mars XTX [Radeon HD 8790M]</td><td>[1002:6606]</td><td>(rev ff)</td></tr>\n"
"<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] Seymour LP [Radeon HD 6430M]</td><td>[1002:6761]</td><td></td></tr>\n"
"<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] Cedar [Radeon HD 5000/6000/7350/8350 Series]</td><td>[1002:68f9]</td><td></td></tr>\n"
"<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] Broadway PRO [Mobility Radeon HD 5850]</td><td>[1002:68a1]</td><td></td></tr>\n"
"<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] RV730/M96 [Mobility Radeon HD 4650/5165]</td><td>[1002:9480]</td><td></td></tr>\n"
"<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] Device [1002:98e4]</td><td>[1002:98e4]</td><td>(rev da)</td></tr>\n"
"</table>\n"
msgstr ""

#. type: Plain text
msgid ""
"For some models, adding `radeon.modeset=0` to the [[boot options|/doc/"
"first_steps/startup_options/#boot_loader_menu]] fixes the issue."
msgstr ""

#. type: Plain text
msgid "We need more test results from users: [[!tails_ticket 12482]]"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"radeon-r9\"></a>\n"
msgstr "<a id=\"radeon-r9\"></a>\n"

#. type: Title -
#, no-wrap
msgid "AMD Radeon R9\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<!--\n"
"Tickets: #12218 #11850\n"
"-->\n"
msgstr ""
"<!--\n"
"Tickets: #12218 #11850\n"
"-->\n"

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>\n"
"<tr><td>Advanced Micro Devices, Inc. [AMD/ATI] Hawaii PRO [Radeon R9 290/390]</td><td>[1002:67b1]</td><td></td></tr>\n"
"</table>\n"
msgstr ""

#. type: Plain text
msgid ""
"Adding `radeon.dpm=0` to the [[boot options|/doc/first_steps/"
"startup_options#boot_loader_menu]] fixes the issue."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"intel\"></a>\n"
msgstr "<a id=\"intel\"></a>\n"

#. type: Title -
#, no-wrap
msgid "Intel\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "<!--\n"
#| "Ticket: #12219\n"
#| "-->\n"
msgid ""
"<!--\n"
"Ticket: #12219\n"
"Ticket: #16224\n"
"-->\n"
msgstr ""
"<!--\n"
"Ticket: #12219\n"
"-->\n"

#. type: Plain text
msgid "Various Intel graphics card."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>\n"
"<tr><td>Intel Corporation</td><td></td><td></td></tr>\n"
"</table>\n"
msgstr ""

#. type: Plain text
msgid ""
"Try adding to the [[boot options|/doc/first_steps/"
"startup_options#boot_loader_menu]], one after the other:"
msgstr ""

#. type: Bullet: '* '
msgid "`xorg-driver=intel`"
msgstr ""

#. type: Bullet: '* '
msgid "`nomodeset`"
msgstr ""

#. type: Bullet: '* '
msgid "`nomodeset xorg-driver=vesa`"
msgstr ""

#. type: Bullet: '* '
msgid "`xorg-driver=modesetting`"
msgstr ""

#. type: Plain text
msgid "Otherwise, try starting in the *Troubleshooting Mode*."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"intel-855GM\"></a>\n"
msgstr "<a id=\"intel-855GM\"></a>\n"

#. type: Title -
#, no-wrap
msgid "Intel 855GM\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<!--\n"
"Ticket: #11096, Debian #776911\n"
msgstr ""
"<!--\n"
"Ticket: #11096, Debian #776911\n"

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>\n"
"<tr><td>Intel Corporation 82852/855GM Integrated Graphics Device</td><td>[8086:3582]</td><td>(rev 02)</td></tr>\n"
"</table>\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<a id=\"nvidia-pascal\"></a>\n"
msgid "<a id=\"nvidia-tesla\"></a>\n"
msgstr "<a id=\"nvidia-pascal\"></a>\n"

#. type: Title -
#, no-wrap
msgid "Nvidia NV50 family (Tesla)\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "<!--\n"
#| "Ticket: #15116\n"
#| "-->\n"
msgid ""
"<!--\n"
"Ticket: #15491\n"
"-->\n"
msgstr ""
"<!--\n"
"Ticket: #15116\n"
"-->\n"

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>\n"
"<tr><td>NVIDIA Corporation MCP89 (GeForce 320M)</td><td>[10de:08a0]</td><td>a2</td></tr>\n"
"</table>\n"
msgstr ""

#. type: Plain text
msgid ""
"Try adding `nomodeset` to the [[boot options|doc/first_steps/"
"startup_options#boot_loader_menu]]."
msgstr ""

#. type: Title ###
#, no-wrap
msgid "Other possibly affected graphics cards"
msgstr ""

#. type: Plain text
msgid ""
"Other graphics cards in the [NV50 family (Tesla)](https://nouveau."
"freedesktop.org/wiki/CodeNames/#nv50familytesla) might be affected:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<!--\n"
"Update this table from time to time as the list of Nvidia cards might change.\n"
"Last updated: 2019-07-30\n"
"-->\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"        <tr>\n"
"            <th><strong>Code name</strong>  </th>\n"
"            <th><strong>Official Name</strong> </th>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV50 (G80)  </td>\n"
"            <td>GeForce 8800 (GTS, GTX, Ultra)<br/>Quadro FX (4600 (SDI), 5600) </td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV84 (G84)  </td>\n"
"            <td>GeForce 8600 (GT, GTS, M GT, M GS), 8700M GT, GeForce 9500M GS, 9650M GS  <br/>Quadro FX (370, 570, 570M, 1600M, 1700), NVS 320M </td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV86 (G86)  </td>\n"
"            <td>GeForce 8300 GS, 8400 (GS, M G, M GS, M GT), 8500 GT, GeForce 9300M G  <br/>Quadro FX 360M, NVS (130M, 135M, 140M, 290) </td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV92 (G92)  </td>\n"
"            <td>GeForce 8800 (GT, GS, GTS 512, M GTS, M GTX)  <br/> GeForce 9600 GSO, 9800 (GT, GTX, GTX+, GX2, M GT, M GTX)  <br/> GeForce GTS 150(M), GTS 160M, GTS 240, GTS 250, GTX (260M, 280M, 285M), GT (330, 340)  <br/> Quadro FX (2800M, 3600M, 3700, 3700M, 3800M, 4700 X2), VX 200 </td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV94 (G94)  </td>\n"
"            <td>GeForce 9600 (GSO 512, GT, S), 9700M GTS, 9800M GTS, GeForce G 110M, GT 130(M), GT 140  <br/>Quadro FX (1800, 2700M) </td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV96 (G96)  </td>\n"
"            <td>GeForce 9400 GT, 9500 (GT, M G), 9600 (M GS, M GT), 9650M GT, 9700M GT  <br/> GeForce G 102M, GT 120  <br/> Quadro FX (380, 580, 770M, 1700M) </td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV98 (G98)  </td>\n"
"            <td>GeForce 8400 GS, GeForce 9200M GS, 9300 (GE, GS, M GS)<br/> GeForce G 100, G 105M <br/>Quadro FX (370 LP, 370M), NVS (150M, 160M, 295, 420, 450) </td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NVA0 (GT200)  </td>\n"
"            <td>GeForce GTX (260, 275, 280, 285, 295)  <br/>Quadro CX, FX (3800, 4800, 5800) </td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NVA3 (GT215)  </td>\n"
"            <td>GeForce GT (240, 320, 335M), GTS (250M, 260M, 350M, 360M) <br/>Quadro FX 1800M </td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NVA5 (GT216)  </td>\n"
"            <td>GeForce GT (220, 230M, 240M, 325M, 330M), 315  <br/>Quadro 400, FX 880M, NVS 5100M </td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NVA8 (GT218)  </td>\n"
"            <td>GeForce 8400 GS, ION 2, GeForce 205, 210, G 210M, 305M, 310(M), 405  <br/>Quadro FX (380 LP, 380M), NVS (300, 2100M, 3100M) </td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NVAA (MCP77/MCP78)  </td>\n"
"            <td>GeForce 8100, 8200, 8300 mGPU / nForce 700a series, 8200M G </td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NVAC (MCP79/MCP7A)  </td>\n"
"            <td>ION, GeForce 9300, 9400 mGPU / nForce 700i series, 8200M G, 9100M, 9400M (G) </td>\n"
"        </tr>\n"
"</table>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"nvidia-maxwell\"></a>\n"
msgstr "<a id=\"nvidia-maxwell\"></a>\n"

#. type: Title -
#, no-wrap
msgid "Nvidia NV110 family (Maxwell)\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<!--\n"
"Ticket: #15116\n"
"-->\n"
msgstr ""
"<!--\n"
"Ticket: #15116\n"
"-->\n"

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>\n"
"<tr><td>NVIDIA Corporation GM107 [GeForce GTX 750 Ti]</td><td>?</td><td>a2</td></tr>\n"
"<tr><td>NVIDIA Corporation GM204M [GeForce GTX 970M]</td><td>[10de:13d8]</td><td></td></tr>\n"
"<tr><td>NVIDIA Corporation GM204M [GeForce GTX 970M]</td><td>[10de:1618]</td><td></td></tr>\n"
"</table>\n"
msgstr ""

#. type: Plain text
msgid "This problem has been fixed for some of these graphic cards."
msgstr ""

#. type: Plain text
msgid ""
"Otherwise, try adding `nouveau.noaccel=1` or `nouveau.modeset=0` to the "
"[[boot options|doc/first_steps/startup_options#boot_loader_menu]]."
msgstr ""

#. type: Plain text
msgid "We need more test results from users: [[!tails_ticket 15116]]"
msgstr ""

#. type: Plain text
msgid ""
"Other graphics cards in the [NV110 family (Maxwell)](https://nouveau."
"freedesktop.org/wiki/CodeNames/#nv110familymaxwell) might be affected:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"        <tr>\n"
"            <th><strong>Code name</strong>  </th>\n"
"            <th><strong>Official Name</strong> </th>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV117 (GM107)  </td>\n"
"            <td>GeForce GTX (745, 750, 840M, 845M, 850M, 860M, 950M, 960M) <br/>Quadro K620, K1200, K2200, M1000M, M1200M; GRID M30, M40</td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV118 (GM108)  </td>\n"
"            <td>GeForce 830M, 840M, 930M, 940M[X]</td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV120 (GM200)  </td>\n"
"            <td>GeForce GTX Titan X</td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV124 (GM204)  </td>\n"
"            <td>GeForce GTX (980)</td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV126 (GM206)  </td>\n"
"            <td>GeForce GTX (950, 960)</td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV12B (GM20B)  </td>\n"
"            <td>Tegra X1</td>\n"
"        </tr>\n"
"</table>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"nvidia-pascal\"></a>\n"
msgstr "<a id=\"nvidia-pascal\"></a>\n"

#. type: Title -
#, no-wrap
msgid "Nvidia NV130 family (Pascal)\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>\n"
"<tr><td>$VENDOR $DEVICE</td><td>[$VENDOR_ID:$PRODUCT_ID]</td><td>(rev $REVISION_NUMBER)</td></tr>\n"
"</table>\n"
"-->\n"
msgstr ""

#. type: Title ###
#, no-wrap
msgid "Possibly affected graphics cards"
msgstr ""

#. type: Plain text
msgid ""
"Graphics cards in the [NV130 family (Pascal)](https://nouveau.freedesktop."
"org/wiki/CodeNames/#nv130familypascal) might be affected:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"        <tr>\n"
"            <th><strong>Code name</strong>  </th>\n"
"            <th><strong>Official Name</strong> </th>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV132 (GP102)  </td>\n"
"            <td>NVIDIA Titan (X, Xp), GeForce GTX 1080 Ti</td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV134 (GP104)  </td>\n"
"            <td>GeForce GTX (1070, 1080)</td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV136 (GP106)  </td>\n"
"            <td>GeForce GTX 1060</td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV137 (GP107)  </td>\n"
"            <td>GeForce GTX (1050, 1050 Ti)</td>\n"
"        </tr>\n"
"        <tr>\n"
"            <td>NV138 (GP108)  </td>\n"
"            <td>GeForce GT 1030</td>\n"
"        </tr>\n"
"</table>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Other issues\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "Black screen with switchable graphics computers\n"
msgstr ""

#. type: Plain text
msgid ""
"Some computers with switchable graphics (such as Optimus) fail to choose a "
"video card and end up on a black screen. This has been reported for MacBook "
"Pro 6,2, MacBook Pro 10,1 Retina, MacBook Pro 15-inch (early 2011) and might "
"affect many others."
msgstr ""
"بعضی رایانه‌ها با گرافیک‌های قابل‌تغییر (مانند اوپتیموس) نمی‌توانند کارت تصویر "
"را انتخاب کنند و تنها یک صفحهٔ سیاه نشان می‌دهند. این مشکل در مورد MacBook Pro "
"6,2، MacBook Pro 10,1 Retin، MacBook Pro 15-inch (اوایل ۲۰۱۱) گزارش شده و "
"ممکن است مدل‌های بسیار دیگری نیز این مشکل را داشته باشند."

#. type: Plain text
msgid "There are several possible workarounds for this issue:"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Explicitly select one of the two graphics adapters in the BIOS instead of "
"letting the system choose one automatically. If this does not solve the "
"problem, try selecting the other graphics adapter."
msgstr ""

#. type: Bullet: '* '
msgid ""
"For the Mac computers, it is possible to use a third-party application, "
"<http://gfx.io/>, to force integrated graphics only through macOS.  Then "
"restart in that special mode that works with Tails."
msgstr ""
"برای رایانه‌های مک، تنها در اواس ده می‌توان از یک ابزار طرف سوم، <http://gfx."
"io/>، برای استفاده از گرافیک‌های یکپارچه استفاده کرد. سپس باید رایانه را در "
"حالت خاصی راه‌اندازی کرد که با تیلز کار می‌کند."

#. type: Bullet: '* '
msgid "Expert Linux users can also do the following:"
msgstr "کاربران خبرهٔ لینوکس نیز می‌توانند این کار را انجام دهند:"

#. type: Bullet: '  1. '
msgid ""
"Add the `i915.modeset=0 rootpw=pass` option in the [[Boot Loader Menu|doc/"
"first_steps/startup_options#boot_loader_menu]]."
msgstr ""

#. type: Bullet: '  2. '
msgid ""
"Create a file `/etc/X11/xorg.conf.d/switchable.conf` with the following "
"content:"
msgstr "فایل `/etc/X11/xorg.conf.d/switchable.conf` با این محتویات ایجاد کنید:"

#. type: Plain text
#, no-wrap
msgid ""
"         Section \"Device\"\n"
"             Identifier \"Device0\"\n"
"             Driver \"nouveau\"\n"
"             BusID \"1:0:0\"\n"
"         EndSection\n"
msgstr ""
"         Section \"Device\"\n"
"             Identifier \"Device0\"\n"
"             Driver \"nouveau\"\n"
"             BusID \"1:0:0\"\n"
"         EndSection\n"

#. type: Bullet: '  4. '
msgid "Restart X with the command:"
msgstr "X را با این فرمان دوباره راه‌اندازی کنید:"

#. type: Plain text
#, no-wrap
msgid "         service gdm3 restart\n"
msgstr "         service gdm3 restart\n"

#. type: Bullet: '  5. '
msgid ""
"After the GNOME session has started, change again the root password with the "
"command:"
msgstr ""
"پس از آغاز شدن نشست کاری گنوم، دوباره گذرواژهٔ اصلی را با این فرمان عوض کنید:"

#. type: Plain text
#, no-wrap
msgid "         sudo passwd\n"
msgstr "         sudo passwd\n"

#. type: Plain text
msgid ""
"For more details, see our ticket on [[!tails_ticket 7505 desc=\"Video is "
"broken with switchable graphics\"]]."
msgstr ""
"برای جزییات بیشتر به این مورد مراجعه کنید: [[!tails_ticket 7505 desc=\"Video "
"is broken with switchable graphics\"]]."

#. type: Plain text
#, no-wrap
msgid "<a id=sg-segfault></a>\n"
msgstr "<a id=sg-segfault></a>\n"

#. type: Title -
#, no-wrap
msgid "Cannot start GNOME session with switchable graphics computers\n"
msgstr ""

#. type: Plain text
msgid ""
"On some computers with switchable graphics, Tails 2.10 and later fails to "
"start the GNOME session and keeps returning to [[Tails Greeter|doc/"
"first_steps/startup_options#greeter]]."
msgstr ""

#. type: Plain text
msgid ""
"Starting in *Troubleshooting Mode* works, as well as adding the `modprobe."
"blacklist=nouveau` to the [[boot options|doc/first_steps/"
"startup_options#boot_loader_menu]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"intel-gm965\"></a>\n"
msgstr "<a id=\"intel-gm965\"></a>\n"

#. type: Title -
#, no-wrap
msgid "Intel GM965/GL960\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<!--\n"
"Ticket: #12217, Linux #187001\n"
"-->\n"
msgstr ""
"<!--\n"
"Ticket: #12217, Linux #187001\n"
"-->\n"

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"<tr><th>Name</th><th>ID</th><th>Revision number</th></tr>\n"
"<tr><td>Intel Corporation Mobile GM965/GL960 Integrated Graphics Controller (primary)</td><td>[8086:2a02]</td><td></td></tr>\n"
"<tr><td>Intel Corporation Mobile GM965/GL960 Integrated Graphics Controller (secondar)</td><td>[8086:2a03]</td><td></td></tr>\n"
"</table>\n"
msgstr ""

#. type: Plain text
msgid "The laptop crashes while running Tails."
msgstr ""

#. type: Plain text
msgid ""
"Adding `video=SVIDEO-1:d` to the [[boot options|/doc/first_steps/"
"startup_options/#boot_loader_menu]] fixes the issue."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"qemu\"></a>\n"
msgstr "<a id=\"qemu\"></a>\n"

#. type: Title -
#, no-wrap
msgid "Virtual machines with *virt-manager*, *libvirt* and *QEMU*\n"
msgstr ""

#. type: Plain text
msgid ""
"See the [[dedicated troubleshooting documentation|doc/advanced_topics/"
"virtualization/virt-manager#graphics-issues]] about graphics issues in Tails "
"running inside a virtual machine with *virt-manager*."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"vmware\"></a>\n"
msgstr "<a id=\"vmware\"></a>\n"

#. type: Title -
#, no-wrap
msgid "Virtual machines with *VMware*\n"
msgstr ""

#. type: Plain text
msgid ""
"To improve support of Tails running inside a virtual machine with *VMware*, "
"[[install|doc/first_steps/additional_software]] the `open-vm-tools-desktop` "
"software package in Tails."
msgstr ""

#, fuzzy
#~| msgid "<a id=\"qemu\"></a>\n"
#~ msgid "<a id=\"lockup\"></a>\n"
#~ msgstr "<a id=\"qemu\"></a>\n"

#~ msgid "<a id=\"sg-black-screen\"></a>\n"
#~ msgstr "<a id=\"sg-black-screen\"></a>\n"
