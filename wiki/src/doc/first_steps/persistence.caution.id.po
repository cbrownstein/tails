# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2013-10-14 14:12+0300\n"
"PO-Revision-Date: 2019-10-24 10:31+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: Tails Translators <tails-l10n@boum.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.20\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p><strong>The use of a persistent volume in a system which is designed to provide\n"
"anonymity and leave no trace is a complicated issue.</strong><br/>\n"
"[[Read carefully the warning section.|persistence/warnings]]</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"
