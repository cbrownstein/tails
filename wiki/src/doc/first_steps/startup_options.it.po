# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails italiano\n"
"POT-Creation-Date: 2018-01-23 18:26+0100\n"
"PO-Revision-Date: 2017-05-30 18:09+0200\n"
"Last-Translator: Zeyev <tbd@tbd.com>\n"
"Language-Team: ita <transitails@inventati.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Startup options\"]]\n"
msgstr "[[!meta title=\"Opzioni di avvio\"]]\n"

#. type: Plain text
msgid ""
"When starting Tails, you can specify startup options to alter some of its "
"basic functioning. The two ways of specifying startup options are the "
"following:"
msgstr ""
"Quando avvii Tails puoi definire le opzioni di avvio per alterare alcuni "
"aspetti del suo funzionamento. I due modi di definire le opzioni di avvio "
"sono i seguenti:"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr "[[!toc levels=2]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"<a id=\"boot_loader_menu\"></a>\n"
"<a id=\"boot_menu\"></a> <!-- for backward compatibility -->\n"
msgstr ""
"<a id=\"boot_loader_menu\"></a>\n"
"<a id=\"boot_menu\"></a> <!-- for backward compatibility -->\n"

#. type: Title =
#, no-wrap
msgid "Using the <span class=\"application\">Boot Loader Menu</span>\n"
msgstr "Utilizzare il <span class=\"application\">Menù di avvio</span>\n"

#. type: Plain text
#, no-wrap
msgid ""
"The <span class=\"application\">Boot Loader Menu</span> is the first screen to appear\n"
"when Tails starts.\n"
msgstr ""
"Il <span class=\"application\">Menù di avvio</span> è la prima schermata che appare\n"
"quando si avvia Tails.\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>The <span class=\"guilabel\">Troubleshooting Mode</span> disables some features of the\n"
"Linux kernel and might work better on some computers. You can try this option if you\n"
"think you are experiencing errors related to hardware compatibility while\n"
"starting Tails.</p>\n"
msgstr ""
"<p>La modalità <span class=\"guilabel\">Troubleshooting Mode</span> disattiva alcune funzionalità\n"
"del kernel e potrebbe funzionare meglio su certi computer. Puoi provare questa opzione\n"
"se pensi di stare incontrando errori legati alla compatibilità dell'hardware quando\n"
"avvii Tails.</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Bullet: '1.  '
msgid ""
"To add a boot option, press <span class=\"keycap\">Tab</span> when the <span "
"class=\"application\">Boot Loader Menu</span> appears. A list of boot "
"options appears at the bottom of the screen."
msgstr ""
"Per aggiungere un'opzione di avvio premi <span class=\"keycap\">Tab</span> "
"quando appare il <span class=\"application\">Menù di avvio</span>. Una lista "
"di opzioni apparirà nella parte bassa dello schermo."

#. type: Plain text
#, no-wrap
msgid ""
"[[!img boot-menu-with-options.png link=no alt=\"Black screen with Tails\n"
"artwork. Boot Loader Menu with two options 'Tails' and 'Tails (Troubleshooting Mode)'.\n"
"At the bottom, a list of options ending with 'vsyscall=none quiet_'\"]]\n"
msgstr ""
"[[!img boot-menu-with-options.png link=no alt=\"Schermo nero con l'illustrazione di\n"
"Tails. 'Boot menu' con due opzioni 'Live' e 'Live (Troubleshooting Mode)'. In basso,\n"
"una lista di opzioni che termina con 'vsyscall=none quiet_'\"]]\n"

#. type: Bullet: '2.  '
msgid ""
"Press <span class=\"keycap\">Space</span>, and type the boot option that you "
"want to add."
msgstr ""
"Premi <span class=\"keycap\">Spazio</span> e digita l'opzione di avvio che "
"vuoi aggiungere."

#. type: Bullet: '3.  '
msgid ""
"If you want to add more than one boot option, type them one after the other, "
"and separate them by a <span class=\"keycap\">Space</span>."
msgstr ""
"Se vuoi aggiungere più di una opzione di avvio, digitale una dopo l'altra "
"separandole con uno <span class=\"keycap\">Spazio</span>"

#. type: Bullet: '4.  '
msgid "Then press <span class=\"keycap\">Enter</span> to start Tails."
msgstr "Poi premi <span class=\"keycap\">Invio</span> per avviare Tails."

#. type: Plain text
#, no-wrap
msgid ""
"<a id=\"greeter\"></a>\n"
"<a id=\"tails_greeter\"></a>\n"
msgstr ""
"<a id=\"greeter\"></a>\n"
"<a id=\"tails_greeter\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Using <span class=\"application\">Tails Greeter</span>\n"
msgstr "Usare <span class=\"application\">Tails Greeter</span>\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "<span class=\"application\">Tails Greeter</span> is the set of dialogs that\n"
#| "appear after the <span class=\"application\">Boot Loader Menu</span>, but before the\n"
#| "<span class=\"application\">GNOME Desktop</span> appears. This is how the first\n"
#| "screen of <span class=\"application\">Tails Greeter</span> looks like:\n"
msgid ""
"<span class=\"application\">Tails Greeter</span>\n"
"appears after the <span class=\"application\">Boot Loader Menu</span>, but before the\n"
"<span class=\"application\">GNOME Desktop</span>:\n"
msgstr ""
"<span class=\"application\">Tails Greeter</span> è un insieme di finestre di dialogo\n"
"che appaiono dopo il <span class=\"application\">Menù di avvio</span>, ma prima che il\n"
"<span class=\"application\">desktop GNOME</span> appaia. \n"
"La prima schermata di <span class=\"application\">Tails Greeter</span> dovrebbe essere così:\n"

# Più opzioni? Pulsante Si/No e Login
#. type: Plain text
#, fuzzy, no-wrap
#| msgid "[[!img tails-greeter-welcome-to-tails.png link=no alt=\"Welcome to Tails!\"]]\n"
msgid "[[!img tails-greeter-welcome-to-tails.png link=no alt=\"Tails Greeter: Welcome to Tails!\"]]\n"
msgstr "[[!img tails-greeter-welcome-to-tails.png link=no alt=\"Benvenuto su Tails.\"]]\n"

#. type: Plain text
msgid ""
"You can activate assistive technologies, like a screen reader or large text, "
"from the universal access menu (the"
msgstr ""
"Puoi attivare tecnologie assistive, come lettori dello schermo o font "
"larghi, dal menu di accesso universale ( l'"

#. type: Plain text
#, no-wrap
msgid "[[!img lib/preferences-desktop-accessibility.png alt=\"Universal Access\" class=\"symbolic\" link=\"no\"]]\n"
msgstr "[[!img lib/preferences-desktop-accessibility.png alt=\"Universal Access\" class=\"symbolic\" link=\"no\"]]\n"

#. type: Plain text
msgid "icon which looks like a person) in the top bar."
msgstr "icona che assomiglia ad una persona) nella barra in alto."

#. type: Plain text
#, no-wrap
msgid ""
"To start Tails without options, click on the\n"
"<span class=\"button\">Start Tails</span> button.\n"
msgstr ""
"**Per avviare Tails senza opzioni**, cliccare sul pulsante\n"
"<span class=\"button\">Start Tails</span>.\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"locale\"></a>\n"
msgstr "<a id=\"locale\"></a>\n"

#. type: Title -
#, no-wrap
msgid "Language & region\n"
msgstr "Lingua e zone geografica\n"

#. type: Plain text
#, no-wrap
msgid ""
"You can configure Tails depending on your language and location from\n"
"<span class=\"application\">Tails Greeter</span>.\n"
msgstr ""
"Puoi configurare Tails secondo la tua lingua e posizione usando \n"
"<span class=\"application\">Tails Greeter</span>:\n"

#. type: Plain text
#, no-wrap
msgid "[[!img locale.png link=\"no\" alt=\"Language & Region section of Tails Greeter\"]]\n"
msgstr "[[!img locale.png link=\"no\" alt=\"Language & Region section of Tails Greeter\"]]\n"

#. type: Bullet: '* '
msgid ""
"The <span class=\"guilabel\">Language</span> option allows you to change the "
"main language of the interface."
msgstr ""
"L'opzione  <span class=\"guilabel\">Lingua</span> ti permette di cambiare il "
"linguaggio principale dell'interfaccia."

#. type: Plain text
#, no-wrap
msgid ""
"  Text that is not translated yet will appear in English. You can [[help\n"
"  to translate more text|contribute/how/translate]].\n"
msgstr ""
"Il testo che non è stato ancora tradotto apparirà in Inglese. Puoi  [[aiutare\n"
"  a tradurre più testo|contribute/how/translate]].\n"

#. type: Bullet: '* '
msgid ""
"The <span class=\"guilabel\">Keyboard Layout</span> option allows you to "
"change the layout of the keyboard. For example to switch to an *AZERTY* "
"keyboard which is common in France."
msgstr ""
"L'opizione <span class=\"guilabel\">Keyboard Layout</span> ti permette di "
"cambiare il tipo di tastiera. Per esempio per passare alla tastiera "
"*AZERTY*, molto comune in Francia."

#. type: Plain text
#, no-wrap
msgid ""
"  You will still be able to switch between different keyboard layouts from the\n"
"  desktop after starting Tails.\n"
msgstr ""
"Sarai sempre in grado di cambiare tra differenti stili di tastiera dal \n"
"descktop dopo aver fatto partire Tails\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "  [[!img introduction_to_gnome_and_the_tails_desktop/keyboard.png size=\"267x\" link=\"no\" alt=\"Menu in the top-right corner of the desktop to switch between different keyboard layouts\"]]\n"
msgid "  [[!img introduction_to_gnome_and_the_tails_desktop/keyboard.png link=\"no\" alt=\"Menu in the top-right corner of the desktop to switch between different keyboard layouts\"]]\n"
msgstr "  [[!img introduction_to_gnome_and_the_tails_desktop/keyboard.png size=\"267x\" link=\"no\" alt=\"Menu in the top-right corner of the desktop to switch between different keyboard layouts\"]]\n"

#. type: Bullet: '* '
msgid ""
"The <span class=\"guilabel\">Formats</span> option allows you to change the "
"date and time format, first day of the week, measurement units, and default "
"paper size according to the standards in use in a country."
msgstr ""
"L'opzione <span class=\"guilabel\">Formats</span> ti permette di cambiare il "
"formato di data e l'ora, il primo giorno della settimana, l'unità di misura "
"e la grandezza standard della carta secondo lo standard usato nel tuo paese. "

#. type: Plain text
#, no-wrap
msgid ""
"  For example, the USA and the United Kingdom, two English-speaking countries,\n"
"  have different standards:\n"
msgstr ""
"Per esempio, in USA e in Inghilterra, due paesi che parlano entrambi inglese, \n"
"ci sono standard differenti:\n"
"\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <table>\n"
"  <tr><td></td><td>USA</td><td>United Kingdom</td></tr>\n"
"  <tr><td>Date & time</td><td>3/17/2017 3:56 PM</td><td>17/03/2017 15:56</td></tr>\n"
"  <tr><td>First day of the week</td><td>Sunday</td><td>Monday</td></tr>\n"
"  <tr><td>Unit system</td><td>Imperial</td><td>Metric</td></tr>\n"
"  <tr><td>Paper size</td><td>Letter</td><td>A4</td></tr>\n"
"  </table>\n"
msgstr ""
"  <table>\n"
"  <tr><td></td><td>USA</td><td>Inghilterra</td></tr>\n"
"  <tr><td>Data e ora</td><td>3/17/2017 3:56 PM</td><td>17/03/2017 15:56</td></tr>\n"
"  <tr><td>Primo giorno della settimana</td><td>Sunday</td><td>Monday</td></tr>\n"
"  <tr><td>Unità di misura</td><td>Imperial</td><td>Metric</td></tr>\n"
"  <tr><td>Grandezza dei fogli</td><td>Letter</td><td>A4</td></tr>\n"
"  </table>\n"

#. type: Plain text
#, no-wrap
msgid ""
"  With this option you can also display the calendar in a different language\n"
"  than the main language. For example, to display a US calendar, with weeks\n"
"  starting on Sunday, when the main language is Russian.\n"
msgstr ""
"All'interno di questa opzione puoi anche mostrare il calendario in una lingua differente \n"
"dal linguaggio principale. Per esempio, per mostrare il calendario americano, con settimane\n"
"che cominciano dalla Domenica, quando il linguaggio principale è il russo.\n"
"\n"

#. type: Plain text
#, no-wrap
msgid "  [[!img US_calendar_in_Russian.png link=\"no\" alt=\"\"]]\n"
msgstr "  [[!img US_calendar_in_Russian.png link=\"no\" alt=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"persistence\"></a>\n"
msgstr "<a id=\"persistence\"></a>\n"

#. type: Title -
#, no-wrap
msgid "Encrypted persistence storage\n"
msgstr "Archiviazione cifrata persistente\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "If an [[encrypted persistence storage]] is detected on the USB stick, an\n"
#| "additional section appears in <span class=\"application\">Tails\n"
#| "Greeter</span> below the <span class=\"guilabel\">Language & Region</span>\n"
#| "section:\n"
msgid ""
"If an [[encrypted persistence storage|first_steps/persistence]] is detected on the USB stick, an\n"
"additional section appears in <span class=\"application\">Tails\n"
"Greeter</span> below the <span class=\"guilabel\">Language & Region</span>\n"
"section:\n"
msgstr ""
"Se una viene trovata un [[encrypted persistence storage]] all'interno di una chiavetta USB, una\n"
"sezione aggiuntiva compare in <span class=\"application\">Tails\n"
"Greeter</span> sotto l'opzione <span class=\"guilabel\">Language & Region</span>\n"

#. type: Plain text
#, no-wrap
msgid "[[!img persistence.png link=\"no\" alt=\"\"]]\n"
msgstr "[[!img persistence.png link=\"no\" alt=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"additional\"></a>\n"
msgstr "<a id=\"additional\"></a>\n"

#. type: Title -
#, no-wrap
msgid "Additional settings\n"
msgstr "Configurazioni aggiuntive\n"

#. type: Plain text
#, no-wrap
msgid ""
"Tails is configured with care to be as safe as possible by default. But,\n"
"depending on your situation, you can change one of the following\n"
"settings from <span class=\"application\">Tails Greeter</span>.\n"
msgstr ""
"Tails è configurata con attenzione per essere più sicura possibile. Ma, \n"
"a seconda delle situazioni, puoi cambiare una delle seguenti opzioni\n"
"da <span class=\"application\">Tails Greeter</span>.\n"

#. type: Plain text
#, no-wrap
msgid "[[!img additional.png link=\"no\" alt=\"Additional settings of Tails Greeter\"]]\n"
msgstr "[[!img additional.png link=\"no\" alt=\"Additional settings of Tails Greeter\"]]\n"

#. type: Bullet: '- '
msgid ""
"Set an <span class=\"guilabel\">Administration Password</span> to be able to "
"perform administrative tasks like installing additional software or "
"accessing the internal hard disks of the computer."
msgstr ""
"Inserire una <span class=\"guilabel\">Password Amministrativa</span> per "
"essere in grado di eseguire operazioni amministrative come l'installazione "
"di software aggiuntivo o accedere ai dischi fissi interni del computer."

#. type: Plain text
#, no-wrap
msgid "  [[See our documentation about the administration password.|administration_password]]\n"
msgstr "  [[Guarda la nostra documentazione a proposito della password da amministrratore.|administration_password]]\n"

#. type: Bullet: '- '
msgid ""
"Disable <span class=\"guilabel\">MAC Address Spoofing</span> to prevent "
"connectivity problems with your network interfaces."
msgstr ""
"Disabilitare il <span class=\"guilabel\">MAC Address Spoofing</span> per "
"prevenire problemi di connettività nelle tue interfacce di rete."

#. type: Plain text
#, no-wrap
msgid "  [[See our documentation about MAC address spoofing.|mac_spoofing]]\n"
msgstr "[[Guarda la nostra documentazione sul mascheramento indirizzo Mac|mac_spoofing]]\n"

#. type: Bullet: '- '
msgid ""
"Change the <span class=\"guilabel\">Network Configuration</span> to either:"
msgstr ""
"Cambiare le <span class=\"guilabel\">Network Configuration</span> per: "

#. type: Bullet: '  - '
msgid "Connect directly to the Tor network (default)."
msgstr "Connettersi direttamente alla rete TOR (predefinito)."

#. type: Bullet: '  - '
msgid "Configure a Tor bridge or local proxy:"
msgstr "Configurare un bridge TOR o un proxy locale:"

#. type: Bullet: '    - '
msgid ""
"If you want to use Tor bridges because your Internet connection is censored "
"or you want to hide the fact that you are using Tor."
msgstr ""
"Se vuoi usare un bridge TOR perchè la tua connessione Internet è censurata o "
"vuoi nascondere il fatto che stai usando Tor."

#. type: Bullet: '    - '
msgid "If you need to use a local proxy to access the Internet."
msgstr "Se necessiti di un proxy locale per accedere a Internet."

#. type: Plain text
#, no-wrap
msgid ""
"    After starting Tails and connecting to a network, an assistant will\n"
"    guide you through the configuration of Tor.\n"
msgstr ""
"  Dopo aver fatto partire Tails ed esserti collegato alla rete, un assistente ti\n"
"guiderà attraverso la configurazione di Tor.\n"

#. type: Plain text
#, no-wrap
msgid "    [[See our documentation about Tor bridges.|bridge_mode]]\n"
msgstr ""
"    [[Guarda la nostra documentazione sui Tor bridges.|bridge_mode]]\n"
"\n"

#. type: Bullet: '  - '
msgid ""
"Disable all networking if you want to work completely offline with "
"additional security."
msgstr ""
"Disabilita tutte le connessioni se vuoi lavorare completamente disconnesso "
"per una sicurezza maggiore."

#. type: Title -
#, no-wrap
msgid "Keyboard shortcuts\n"
msgstr "Scorciatoie da tastiera (Keyboard shortcuts)\n"

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"<tr><td><span class=\"keycap\">Alt+L</span></td><td><span class=\"guilabel\">Language</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+K</span></td><td><span class=\"guilabel\">Keyboard Layout</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+F</span></td><td><span class=\"guilabel\">Formats</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+P</span></td><td><span class=\"guilabel\">Encrypted Persistent Storage</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+A</span></td><td><span class=\"guilabel\">Additional Settings</td></tr>\n"
"<tr><td><span class=\"keycap\">Ctrl+Shift+A</span></td><td><span class=\"guilabel\">Administration Password</td></tr>\n"
"<tr><td><span class=\"keycap\">Ctrl+Shift+M</span></td><td><span class=\"guilabel\">MAC Address Spoofing</td></tr>\n"
"<tr><td><span class=\"keycap\">Ctrl+Shift+N</span></td><td><span class=\"guilabel\">Network Configuration</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+S</td><td><span class=\"guilabel\">Start Tails</td></tr>\n"
"</table>\n"
msgstr ""
"<table>\n"
"<tr><td><span class=\"keycap\">Alt+L</span></td><td><span class=\"guilabel\">Language</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+K</span></td><td><span class=\"guilabel\">Keyboard Layout</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+F</span></td><td><span class=\"guilabel\">Formats</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+P</span></td><td><span class=\"guilabel\">Encrypted Persistent Storage</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+A</span></td><td><span class=\"guilabel\">Additional Settings</td></tr>\n"
"<tr><td><span class=\"keycap\">Ctrl+Shift+A</span></td><td><span class=\"guilabel\">Administration Password</td></tr>\n"
"<tr><td><span class=\"keycap\">Ctrl+Shift+M</span></td><td><span class=\"guilabel\">MAC Address Spoofing</td></tr>\n"
"<tr><td><span class=\"keycap\">Ctrl+Shift+N</span></td><td><span class=\"guilabel\">Network Configuration</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+S</td><td><span class=\"guilabel\">Start Tails</td></tr>\n"
"</table>\n"

#~ msgid ""
#~ "**To start Tails in languages other than English**, select the one you\n"
#~ "want from the menu at the bottom of the screen. You can also adapt\n"
#~ "your country and keyboard layout. When you do that, <span class="
#~ "\"application\">Tails Greeter</span> itself\n"
#~ "switches language.\n"
#~ msgstr ""
#~ "**Per avviare Tails in una lingua diversa dall'inglese**, seleziona "
#~ "quella\n"
#~ "desiderata dal menù sul fondo dello schermo. Puoi anche adattare\n"
#~ "il layout del tuo paese e della tua tastiera. Una volta fatto, <span "
#~ "class=\"application\">Tails Greeter</span> stesso\n"
#~ " cambierà lingua.\n"

#~ msgid ""
#~ "**To set more options**, click on the <span class=\"button\">Yes</span> "
#~ "button.\n"
#~ "Then click on the <span class=\"button\">Forward</span> button.\n"
#~ msgstr ""
#~ "**Per inserire più opzioni**, clicca sul pulsante <span class=\"button"
#~ "\">Sì</span>.\n"
#~ "Poi clicca sul pulsante <span class=\"button\">Indietro</span>.\n"

#~ msgid ""
#~ "  - [[Network configuration|network_configuration]]\n"
#~ "    - [[Tor bridge mode|bridge_mode]]\n"
#~ "    - [[Disabling all networking (offline_mode)|offline_mode]]\n"
#~ "  - [[Encrypted persistence|doc/first_steps/persistence/use]]\n"
#~ msgstr ""
#~ "  - [[Configurazioni di rete|network_configuration]]\n"
#~ "    - [[Modalità bridge di Tor|bridge_mode]]\n"
#~ "    - [[Disabilitare tutte le connessioni di rete(offline_mode)|"
#~ "offline_mode]]\n"
#~ "  - [[Persistenza criptata|doc/first_steps/persistence/use]]\n"

#, fuzzy
#~| msgid ""
#~| "Here is a list of options that you can add to the <span class="
#~| "\"application\">boot\n"
#~| "menu</span>:\n"
#~ msgid ""
#~ "Here is a list of options that you can add to the <span class="
#~ "\"application\">Boot\n"
#~ "Loader Menu</span>:\n"
#~ msgstr ""
#~ "Qui c'è una lista di opzioni che puoi aggiungere al <span class="
#~ "\"application\">menù\n"
#~ "di avvio</span>:\n"

#~ msgid "Problems booting?\n"
#~ msgstr "Problemi all'avvio ?\n"

#~ msgid ""
#~ "If you have problems booting Tails, please read the [[following "
#~ "guidelines|doc/first_steps/bug_reporting/tails_does_not_start]]."
#~ msgstr ""
#~ "Se hai problemi con l'avvio di Tails puoi leggere le [[seguenti "
#~ "istruzioni|doc/first_steps/bug_reporting/tails_does_not_start]]."

#~ msgid "[[Windows camouflage|windows_camouflage]]"
#~ msgstr "[[Finestra mascherata|windows_camouflage]]"

#~ msgid ""
#~ "<span class=\"command\">truecrypt</span>, to enable [[TrueCrypt|"
#~ "encryption_and_privacy/truecrypt]]"
#~ msgstr ""
#~ "<span class=\"command\">truecrypt</span>, pour activer [[TrueCrypt|"
#~ "encryption_and_privacy/truecrypt]]"

#~ msgid ""
#~ "Problems booting?\n"
#~ "===============================\n"
#~ msgstr ""
#~ "Problèmes au démarrage ?\n"
#~ "===============================\n"
