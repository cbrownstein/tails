# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-05-22 15:30+0000\n"
"PO-Revision-Date: 2020-01-07 15:25+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: Tails translators <tails@boum.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.5.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Support\"]]\n"
msgstr "[[!meta title=\"À l'aide\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"hide-breadcrumbs\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Search the documentation"
msgstr "Chercher dans la documentation"

#. type: Plain text
msgid ""
"Read the [[official documentation|doc]] to learn more about how Tails works "
"and maybe start answering your questions. It contains:"
msgstr ""
"Lisez la [[documentation officielle|doc]] pour en apprendre plus sur comment "
"Tails fonctionne et peut-être commencer à avoir des réponses à vos "
"questions. Elle contient :"

#. type: Bullet: '  - '
msgid "General information about what Tails is"
msgstr "Des informations générales à propos de Tails"

#. type: Bullet: '  - '
msgid ""
"Information to understand how it can help you and what its limitations are"
msgstr ""
"Des informations pour comprendre en quoi Tails peut être utile, et quelles "
"sont ses limites"

#. type: Bullet: '  - '
msgid "Guides covering typical uses of Tails"
msgstr "Des guides des usages typiques de Tails"

#. type: Plain text
msgid "[[Visit Tails documentation|doc]]"
msgstr "[[Consultez la documentation de Tails|doc]]"

#. type: Title =
#, no-wrap
msgid "Learn how to use Tails"
msgstr "Apprendre à utiliser Tails"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"support/learn/intro.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"support/learn/intro.inline.fr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Title =
#, no-wrap
msgid "Frequently asked questions"
msgstr "Foire aux questions"

#. type: Plain text
msgid "Search our list of [[frequently asked questions|faq]]."
msgstr "Cherchez dans notre [[foire aux questions|faq]]."

#. type: Title =
#, no-wrap
msgid "Upgrade"
msgstr "Mise à jour"

#. type: Plain text
msgid ""
"Make sure you are using the latest version, as [[upgrading|doc/upgrade]] "
"might solve your problem."
msgstr ""
"Assurez-vous d'utiliser la dernière version, car [[mettre à jour|doc/"
"upgrade]] peut résoudre votre problème."

#. type: Title =
#, no-wrap
msgid "Check if the problem is already known"
msgstr "Vérifiez si le problème est déjà connu"

#. type: Plain text
msgid "You can have a look at:"
msgstr "Vous pouvez jeter un œil à :"

#. type: Bullet: '  - '
msgid "The [[list of known issues|support/known_issues]]"
msgstr "La liste des [[problèmes connus|support/known_issues]]"

#. type: Bullet: '  - '
msgid ""
"The [list of things that will be in the next release](https://redmine.tails."
"boum.org/code/projects/tails/issues?query_id=327)"
msgstr ""
"La [liste des changements dans la prochaine version](https://redmine.tails."
"boum.org/code/projects/tails/issues?query_id=327)"

#. type: Bullet: '  - '
msgid "The [[!tails_redmine desc=\"rest of our open tickets on Redmine\"]]"
msgstr "Le [[!tails_redmine desc=\"reste des tickets ouverts sur Redmine\"]]"

#. type: Plain text
#, no-wrap
msgid "<div id=\"bugs\" class=\"blocks two-blocks\">\n"
msgstr "<div id=\"bugs\" class=\"blocks two-blocks\">\n"

#. type: Plain text
#, no-wrap
msgid "  <h1>Report an error</h1>\n"
msgstr "  <h1>Signaler une erreur</h1>\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <p>If you are facing an error in Tails, please follow the [[bug reporting\n"
"  guidelines|doc/first_steps/bug_reporting]].</p>\n"
msgstr ""
"  <p>Si vous avez trouvé un bug dans Tails, merci de suivre les [[recommandations\n"
"  pour signaler un bug|doc/first_steps/bug_reporting]].</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <p>If Tails does not start, please see our specific\n"
"  [[reporting guidelines|doc/first_steps/bug_reporting#does_not_start]].</p>\n"
msgstr ""
"  <p>Si Tails ne démarre pas, veuillez suivre les [[instructions\n"
"  spécifiques|doc/first_steps/bug_reporting#does_not_start]].</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "<div id=\"wishlist\" class=\"blocks two-blocks\">\n"
msgstr "<div id=\"wishlist\" class=\"blocks two-blocks\">\n"

#. type: Plain text
#, no-wrap
msgid "  <h1>Request a feature</h1>\n"
msgstr "  <h1>Demander une fonctionnalité</h1>\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <p>If you would like to see a new feature in Tails,\n"
"  search the [[!tails_redmine desc=\"open tickets in Redmine\"]] first,\n"
"  and file a new ticket in there if no existing one matches your needs.</p>\n"
msgstr ""
"  <p>Si vous désirez demander une nouvelle fonctionnalité dans Tails,\n"
"  cherchez auparavant parmi les [[!tails_redmine desc=\"tickets ouverts dans Redmine\"]],\n"
"  et ouvrez un nouveau ticket si aucun ne correspond à votre demande.</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div> <!-- #wishlist -->\n"
msgstr "</div> <!-- #wishlist -->\n"

#. type: Plain text
#, no-wrap
msgid "<div id=\"talk\">\n"
msgstr "<div id=\"talk\">\n"

#. type: Plain text
#, no-wrap
msgid "  <h1>Write to our help desk</h1>\n"
msgstr "  <h1>Écrivez à notre équipe d'assistance</h1>\n"

#. type: Plain text
#, no-wrap
msgid "  [[!inline pages=\"support/talk\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "  [[!inline pages=\"support/talk.fr\" raw=\"yes\" sort=\"age\"]]\n"

#~ msgid "<div id=\"page-found_a_problem\">\n"
#~ msgstr "<div id=\"page-found_a_problem\">\n"

#~ msgid "<div id=\"bugs\">\n"
#~ msgstr "<div id=\"bugs\">\n"

#~ msgid "</div> <!-- #bugs -->\n"
#~ msgstr "</div> <!-- #bugs -->\n"

#~ msgid "</div> <!-- #page-found_a_problem -->\n"
#~ msgstr "</div> <!-- #page-found_a_problem -->\n"

#~ msgid "</div> <!-- #talk -->\n"
#~ msgstr "</div> <!-- #talk -->\n"

#, fuzzy
#~| msgid ""
#~| "The [list of things that will be in the next release](https://labs."
#~| "riseup.net/code/projects/tails/issues?query_id=111)"
#~ msgid ""
#~ "The [rest of our open tickets on Redmine](https://redmine.tails.boum.org/"
#~ "code/projects/tails/issues?set_filter=1)"
#~ msgstr ""
#~ "La [liste des changements dans la prochaine version](https://labs.riseup."
#~ "net/code/projects/tails/issues?query_id=111)"

#~ msgid "The [[list of things to do|todo]]"
#~ msgstr "La liste des [[choses à faire|todo]]"

#~ msgid "</div> <!-- .container -->\n"
#~ msgstr "</div> <!-- .container -->\n"

#~ msgid "</div> <!-- #support -->\n"
#~ msgstr "</div> <!-- #support -->\n"

#~ msgid "How-tos on getting Tails to work"
#~ msgstr "Des tutoriels pour faire fonctionner Tails"

#~ msgid "It contains:"
#~ msgstr "Elle contient :"

#~ msgid "Troubleshooting"
#~ msgstr "Comprendre un problème"

#~ msgid ""
#~ "If you have found an error in Tails or if you would like to see a new "
#~ "feature in it, have a look at the [[support/troubleshooting]] page."
#~ msgstr ""
#~ "Si vous avez trouvé une erreur dans Tails ou si vous voulez y voir une "
#~ "nouvelle fonctionnalité, allez voir la page [[comprendre un problème|"
#~ "support/troubleshooting]] page."

#~ msgid "Found a problem?"
#~ msgstr "Un problème ?"
