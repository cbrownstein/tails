# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-06-02 18:13+0000\n"
"PO-Revision-Date: 2019-07-27 17:25+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 2.20\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Tue, 21 May 2019 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Tue, 21 May 2019 12:34:56 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 3.14 is out\"]]\n"
msgstr "[[!meta title=\"Tails 3.14 est sorti\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
msgid ""
"This release fixes [[many security vulnerabilities|security/"
"Numerous_security_holes_in_3.13.2]]. You should upgrade as soon as possible."
msgstr ""
"Cette version corrige [[plusieurs failles de sécurité|security/"
"Numerous_security_holes_in_3.13.2]]. Vous devriez mettre à jour dès que "
"possible."

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Title #
#, no-wrap
msgid "Changes"
msgstr "Changements"

#. type: Title ##
#, no-wrap
msgid "Upgrades and changes"
msgstr "Mises à jour et changements"

#. type: Bullet: '- '
msgid ""
"Update *Linux* to 4.19.37 and most firmware packages. This should improve "
"the support for newer hardware (graphics, Wi-Fi, etc.)."
msgstr ""
"Mise à jour de *Linux* vers la version 4.19.37 et mise à jour de la plupart "
"des paquets de micrologiciel. Cela doit améliorer le support du nouveau "
"matériel (graphique, Wi-Fi, etc.)."

#. type: Bullet: '- '
msgid ""
"Enable all available mitigations for the MDS (Microarchitectural Data "
"Sampling)  attacks and disable [[!wikipedia Simultaneous_multithreading desc="
"\"SMT\"]] (simultaneous multithreading) on all vulnerable processors to fix "
"the [RIDL, Fallout](https://mdsattacks.com/) and [ZombieLoad](https://"
"zombieloadattack.com/) security vulnerabilities."
msgstr ""
"Activation de toutes les mitigations disponibles pour les attaques MDS "
"(Microarchitectural Data Sampling) et désactivation du [[!wikipedia_fr "
"Simultaneous_multithreading desc=\"SMT\"]] (simultaneous multithreading) sur "
"tous les processeurs vulnérables pour corriger les failles de sécurité "
"[RIDL, Fallout](https://mdsattacks.com/) et [ZombieLoad](https://"
"zombieloadattack.com/)."

#. type: Plain text
msgid "- Update *Tor Browser* to 8.5."
msgstr "- Mise à jour du *Navigateur Tor* vers la version 8.5."

#. type: Plain text
msgid "- Remove the following applications:"
msgstr "- Suppression des applications suivantes :"

#. type: Plain text
#, no-wrap
msgid ""
"  - Desktop applications\n"
"    - *Gobby*\n"
"    - *Pitivi*\n"
"    - *Traverso*\n"
msgstr ""
"  - Applications de bureau\n"
"    - *Gobby*\n"
"    - *Pitivi*\n"
"    - *Traverso*\n"

#. type: Plain text
#, no-wrap
msgid ""
"  - Command-line tools\n"
"    - `hopenpgp-tools`\n"
"    - `keyringer`\n"
"    - `monkeysign`\n"
"    - `monkeysphere`\n"
"    - `msva-perl`\n"
"    - `paperkey`\n"
"    - `pwgen`\n"
"    - `ssss`\n"
"    - `pdf-redact-tools`\n"
msgstr ""
"  - Outils en ligne de commande\n"
"    - `hopenpgp-tools`\n"
"    - `keyringer`\n"
"    - `monkeysign`\n"
"    - `monkeysphere`\n"
"    - `msva-perl`\n"
"    - `paperkey`\n"
"    - `pwgen`\n"
"    - `ssss`\n"
"    - `pdf-redact-tools`\n"

#. type: Plain text
#, no-wrap
msgid ""
"  You can install these applications again using the\n"
"  *[[Additional Software|doc/first_steps/additional_software]]* feature.\n"
msgstr ""
"  Vous pouvez installer à nouveau ces applications en utilisant la fonctionnalité\n"
"  *[[Logiciels additionnels|doc/first_steps/additional_software]]*.\n"

#. type: Plain text
#, no-wrap
msgid ""
"  Thanks to the removal of these less popular applications in 3.14 and the removal\n"
"  of some language packs in 3.13.2, Tails 3.14 is 39 MB smaller than 3.13.\n"
msgstr ""
"  Grâce à la suppression de ces applications moins populaires dans la version 3.14 et à la suppression\n"
"  de certains packs de langue dans la version 3.13.2, Tails 3.14 est plus petit de 39 Mo par rapport à la version 3.13.\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"noscript\"></a>\n"
msgstr "<a id=\"noscript\"></a>\n"

#. type: Title ##
#, no-wrap
msgid "Fixed problems"
msgstr "Problèmes résolus"

#. type: Bullet: '- '
msgid ""
"Add back the *OpenPGP Applet* and *Pidgin* notification icons to the top "
"navigation bar."
msgstr ""
"Ajout à nouveau des icônes de notification de *OpenPGP Applet* et *Pidgin* "
"dans la barre de navigation en haut."

#. type: Plain text
#, no-wrap
msgid "  [[!img version_3.13.2/top-navigation-bar.png link=\"no\"]]\n"
msgstr "  [[!img version_3.13.2/top-navigation-bar.png link=\"no\"]]\n"

#. type: Plain text
msgid "- Fix *NoScript* being deactivated when restarting *Tor Browser*."
msgstr ""
"- Correction de la désactivation de *NoScript* lors du redémarrage du "
"*Navigateur Tor*."

#. type: Plain text
#, no-wrap
msgid ""
"  <div class=\"note\">\n"
"  <p>*NoScript* is removed from the set of icons displayed by default in\n"
"  *Tor Browser*. This is how *Tor Browers* looks in Tails 3.14.</p>\n"
msgstr ""
"  <div class=\"note\">\n"
"  <p>*NoScript* a été enlevé du groupe d'icônes affiché par défaut dans le\n"
"  *Navigateur Tor*. Voici à quoi ressemble le *Navigateur Tor* dans Tails 3.14.</p>\n"

#. type: Plain text
#, no-wrap
msgid "  [[!img tor-browser.png alt=\"\" link=\"no\"]]\n"
msgstr "  [[!img tor-browser.png alt=\"\" link=\"no\"]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <p>To see the list of add-ons that are enabled, choose <span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">[[!img lib/open-menu.png alt=\"\" class=symbolic link=\"no\"]]</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Add-ons</span>.\n"
"  </div>\n"
msgstr ""
"  <p>Pour voir la liste des extensions activées, choisissez <span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">[[!img lib/open-menu.png alt=\"\" class=symbolic link=\"no\"]]</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Modules complémentaires</span>.\n"
"  </div>\n"

#. type: Plain text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog desc=\"changelog"
"\"]]."
msgstr ""
"Pour plus d'informations, lisez notre [[!tails_gitweb debian/changelog desc="
"\"liste des changements\"]]."

#. type: Plain text
#, no-wrap
msgid "<a id=\"known-issues\"></a>\n"
msgstr "<a id=\"known-issues\"></a>\n"

#. type: Title #
#, no-wrap
msgid "Known issues"
msgstr "Problèmes connus"

#. type: Title ###
#, no-wrap
msgid "Tails fails to start a second time on some computers ([[!tails_ticket 16389]])"
msgstr "Tails n'arrive pas à démarrer une seconde fois sur certains ordinateurs ([[!tails_ticket 16389]])"

#. type: Plain text
msgid ""
"On some computers, after installing Tails to a USB stick, Tails starts a "
"first time but fails to start a second time. In some cases, only BIOS "
"(Legacy) was affected and the USB stick was not listed in the Boot Menu."
msgstr ""
"Sur certains ordinateurs, après l'installation de Tails sur une clé USB, "
"Tails démarre une première fois mais n'arrive pas à démarrer une deuxième "
"fois. Dans certains cas, seul le BIOS (Legacy) est affecté et la clé USB "
"n'est pas listée dans le menu de démarrage."

#. type: Plain text
#, no-wrap
msgid ""
"We are still investigating the issue, so if it happens to you, please\n"
"report your findings by email to <tails-testers@boum.org>. Mention the\n"
"model of the computer and the USB stick. This mailing\n"
"list is [archived publicly](https://lists.autistici.org/list/tails-testers.html).\n"
msgstr ""
"Nous continuons d'enquêter sur ce problème, donc si cela vous arrive, veuillez\n"
"nous signaler vos constatations par courriel à <tails-testers@boum.org>. Mentionnez le\n"
"modèle de l'ordinateur et de la clé USB. Celle liste de discussion\n"
"est [archivée publiquement](https://lists.autistici.org/list/tails-testers.html).\n"

#. type: Plain text
msgid "To fix this issue:"
msgstr "Pour corriger ce problème :"

#. type: Bullet: '1. '
msgid "Reinstall your USB stick using the same installation method."
msgstr "Réinstallez votre clé USB en utilisant la même méthode d'installation."

#. type: Bullet: '1. '
msgid ""
"Start Tails for the first time and [[set up an administration password|doc/"
"first_steps/startup_options/administration_password]]."
msgstr ""
"Démarrez Tails une première fois et [[définissez un mot de passe "
"d'administration|doc/first_steps/startup_options/administration_password]]."

#. type: Bullet: '1. '
msgid ""
"Choose <span class=\"menuchoice\"> <span class=\"guimenu\">Applications</"
"span>&nbsp;▸ <span class=\"guisubmenu\">System Tools</span>&nbsp;▸ <span "
"class=\"guimenuitem\">Root Terminal</span> </span> to open a <span class="
"\"application\">Root Terminal</span>."
msgstr ""
"Choisissez <span class=\"menuchoice\"> <span class=\"guimenu\">Applications</"
"span>&nbsp;▸ <span class=\"guisubmenu\">Outils système</span>&nbsp;▸ <span "
"class=\"guimenuitem\">Terminal administrateur</span> </span> pour ouvrir un "
"<span class=\"application\">Terminal administrateur</span>."

#. type: Bullet: '1. '
msgid "Execute the following command:"
msgstr "Exécutez la commande suivante :"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"pre command\">sgdisk --recompute-chs /dev/bilibop</p>\n"
msgstr "   <p class=\"pre command\">sgdisk --recompute-chs /dev/bilibop</p>\n"

#. type: Plain text
msgid "You can also test an experimental image:"
msgstr "Vous pouvez également tester une image expérimentale :"

#. type: Bullet: '1. '
msgid ""
"[Download the *.img* file from our development server](https://nightly.tails."
"boum.org/build_Tails_ISO_bugfix-16389-recompute-chs/lastSuccessful/archive/"
"build-artifacts/)."
msgstr ""
"[Téléchargez le fichier *.img* depuis notre serveur de développement]"
"(https://nightly.tails.boum.org/build_Tails_ISO_bugfix-16389-recompute-chs/"
"lastSuccessful/archive/build-artifacts/)."

#. type: Bullet: '1. '
msgid "Install it using the same installation methods."
msgstr "Installez-le en utilisant les même méthodes d'installation."

#. type: Plain text
#, no-wrap
msgid ""
"   We don't provide any OpenPGP signature or other verification technique\n"
"   for this test image. Please only use it for testing.\n"
msgstr ""
"   Nous ne fournissons pas de signature OpenPGP ou d'autres techniques de vérification\n"
"   pour cette image de test. Veuillez l'utiliser uniquement pour faire des tests.\n"

#. type: Plain text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr ""
"Voir la liste des [[problèmes connus de longue date|support/known_issues]]."

#. type: Title #
#, no-wrap
msgid "Get Tails 3.14"
msgstr "Obtenir Tails 3.14"

#. type: Title ##
#, no-wrap
msgid "To upgrade your Tails USB stick and keep your persistent storage"
msgstr ""
"Pour mettre à jour votre clé USB Tails et conserver votre stockage persistant"

#. type: Plain text
msgid ""
"- Automatic upgrades are available from 3.12, 3.12.1, 3.13, 3.13.1, and "
"3.13.2 to 3.14."
msgstr ""
"- Des mises à jour automatiques sont disponibles depuis les versions 3.12, "
"3.12.1, 3.13, 3.13.1 et 3.13.2 vers la version 3.14."

#. type: Bullet: '- '
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual upgrade|doc/upgrade#manual]]."
msgstr ""
"Si vous ne pouvez pas faire une mise à jour automatique ou si le démarrage "
"échoue après une mise à jour automatique, merci d'essayer de faire une "
"[[mise à jour manuelle|doc/upgrade#manual]]."

#. type: Title ##
#, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "Pour installer Tails sur une nouvelle clé USB"

#. type: Plain text
msgid "Follow our installation instructions:"
msgstr "Suivez nos instructions d'installation :"

#. type: Bullet: '  - '
msgid "[[Install from Windows|install/win]]"
msgstr "[[Installer depuis Windows|install/win]]"

#. type: Bullet: '  - '
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Installer depuis macOS|install/mac]]"

#. type: Bullet: '  - '
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Installer depuis Linux|install/linux]]"

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\"><p>All the data on this USB stick will be lost.</p></div>\n"
msgstr "<div class=\"caution\"><p>Toutes les données sur cette clé USB seront perdues.</p></div>\n"

#. type: Title ##
#, no-wrap
msgid "To download only"
msgstr "Pour seulement télécharger"

#. type: Plain text
msgid ""
"If you don't need installation or upgrade instructions, you can directly "
"download Tails 3.14:"
msgstr ""
"Si vous n'avez pas besoin d'instructions d'installation ou de mise à jour, "
"vous pouvez télécharger directement Tails 3.14 :"

#. type: Bullet: '  - '
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Pour clés USB (image USB)|install/download]]"

#. type: Bullet: '  - '
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Pour DVD et machines virtuelles (image ISO)|install/download-iso]]"

#. type: Title #
#, no-wrap
msgid "What's coming up?"
msgstr "Et ensuite ?"

#. type: Plain text
msgid "Tails 3.15 is [[scheduled|contribute/calendar]] for July 9."
msgstr "Tails 3.15 est [[prévu|contribute/calendar]] pour le 9 juillet."

#. type: Plain text
msgid "Have a look at our [[!tails_roadmap]] to see where we are heading to."
msgstr ""
"Jetez un œil à notre [[!tails_roadmap desc=\"feuille de route\"]] pour "
"savoir ce que nous avons en tête."

#. type: Plain text
#, no-wrap
msgid ""
"We need your help and there are many ways to [[contribute to\n"
"Tails|contribute]] (<a href=\"https://tails.boum.org/donate/?r=3.14\">donating</a> is only one of\n"
"them). Come [[talk to us|about/contact#tails-dev]]!\n"
msgstr ""
"Nous avons besoin de votre aide et il y a de nombreuses manières de [[contribuer à\n"
"Tails|contribute]] (<a href=\"https://tails.boum.org/donate/?r=3.14\">faire un don</a> est seulement l'une\n"
"d'entre elles). Venez [[discuter avec nous|about/contact#tails-dev]] !\n"
