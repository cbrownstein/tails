# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-04-12 16:10+0200\n"
"PO-Revision-Date: 2020-03-07 14:27+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 3.5.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Celebrating 10 years of Tails!\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Mon, 15 Dec 2019 20:00:00 +0000\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
msgid ""
"In 2019, we are especially proud of celebrating with you the 10 years of "
"Tails."
msgstr ""

#. type: Plain text
msgid ""
"The first release of Tails, back then *amnesia*, was announced in 2009.  "
"Since then we released 98 versions of Tails, which were used more than 40 "
"million times."
msgstr ""

#. type: Plain text
msgid ""
"Here are some stories about how it all started and some vintage screenshots. "
"But first of all, the birthday cake!"
msgstr ""
